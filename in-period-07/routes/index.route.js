
module.exports = function (app) {
    // User Routes
    app.use('/user', require('./user.route.js')());
    app.use('/auth', require('./auth.route.js')());
};