var jwt = require('jsonwebtoken');
var fs = require('fs');

var cert = fs.readFileSync(__dirname + '/key/key.pem');
var pub = fs.readFileSync(__dirname + '/key/key.pub');
/*
obj: object muốn mã hóa token
cert: sercet string hoặc key.pem
{ } : jwt option .. algorithm : 'RS256' là chuẩn mã hóa
expiresIn: hiệu lực của token
*/
exports.sign = function (obj) {
  return  new Promise(function(resolve,reject){
        jwt.sign(obj, cert, { 
            algorithm: 'RS256',
            expiresIn: '10h'
        }, function (err, token) {
           if(err){
               reject(err);
           } else {
               resolve(token);
           }
        });
    });

}

/*
token : token client cung cấp
pub: secret string hoặc key.pub
*/
exports.verify = function (token) {
   return new Promise(function(resolve, reject){
        jwt.verify(token, pub, function (err, decoded) {
            if(err){
                reject(err);
            } else {
                resolve(decoded);
            }
        });
    })
   
};
