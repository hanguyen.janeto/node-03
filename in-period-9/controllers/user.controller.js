var userDao = require('../dao/user.dao');
module.exports = {
    deleteUser: deleteUser,
    getUserById : getUserById,
    createUser: createUser,
    updateUser: updateUser,
    getUser : getUser
}
function deleteUser(req, res, next) {
    var id = req.params.id;
    var exist = false;
    userDao.deleteById(id)
    .then(function(rs){
        res.send("ok")
    })
    .catch (function(err){
        next(err);
    })
}

function getUserById(req, res, next) {
    var id = req.params.id;
    userDao.findById(req.params.id)
    .then(function(user){
        res.send(user);
    })
    .catch(function(err){
        next(err);
    })
}

function createUser(req,res,next) {
    var user = req.body;
    
   userDao.create(user)
   .then(function(user){
    res.send(user);
   })
   .catch(function(err){
       next(err);
   })

}

function validate(user) {
    var errors = [];

}

function getUser(req, res, next) {
   userDao.findAll({})
   .then(function(users){
    res.send(users);
   })
   .catch(function(err){
       next(err);
   })
}

function updateUser(req, res) {
    var user = req.body;
    var id = req.params.id;
    console.log(user);

    if (!user.username) {
        res.status(400);
        res.end("Username is required");
    }

    fs.readFile(path.join(__dirname, "../user.json"), "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            var foundUserIndex;

            for (var i = 0; i < formattedData.length; i++) {
                if (formattedData[i].id == id) {
                    foundUserIndex = i;
                    break;
                }
            }

            if (foundUserIndex == undefined) {
                res.status(404);
                res.send("Not Found");
            } else {
                for (var p in user) {
                    formattedData[foundUserIndex][p] = user[p];
                }

                fs.writeFile(path.join(__dirname, "../user.json"), JSON.stringify(formattedData), function (err) {
                    if (err) {
                        res.status(500);
                        res.send(err.message);
                    } else {
                        res.send(formattedData[foundUserIndex]);
                    }
                })
            }
        }
    })
}