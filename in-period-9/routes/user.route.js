var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs');
var path = require('path');
var auth = require('./../middle-ware/auth');
var userController = require('./../controllers/user.controller');
module.exports = function () {
    router.get('/', userController.getUser)
    router.post('/', userController.createUser);
    router.put('/:id',  userController.updateUser);
    router.get('/:id', userController.getUserById);
    router.delete('/:id',  userController.deleteUser);

    return router;
}

