var router = require('express').Router();
var fs = require("fs");
var path = require('path');
var jwt = require('./../utils/jwt');
var userController = require('./../controller/auth.controller');

module.exports = function () {
    router.post('/login', userController.login);
    router.post('/register', userController.register);

    return router;
};

