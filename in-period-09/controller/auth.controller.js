var fs = require('fs');
var path = require('path');
var userDao = require('./../dao/user.dao');
var jwt = require('./../utils/jwt');
var crypto = require('../utils/crypto');

module.exports = {
    register: register
}

function register(req, res, next) {
    var userModel = req.body;
    // kiem tra du lieu nguoi dung
    var errors = validateUserModel(userModel);
    if (errors.length > 0) {
        return next(errors);
    }
    //
    userDao.findOne({
        username: userModel.username
    })
        .then(function (user) {
            if (user) {
                next("username is exist!");
            } else {
                userModel.salt = crypto.genSalt();
                return crypto.hashWithSaltAsync(userModel.password, userModel.salt)
            }
        })
        .then(function (hash) {
            userModel.password = hash;
            return userDao.create(userModel);
        })
        .then(function (user) {
            res.send(user);
        })
        .catch(function (err) {
            next(err);
        })

}

function login(req, res, next) {
    var userModel = req.body;
    var userInDb = {};
    var errors = validateUserModel(userModel);
    if (errors.length > 0) {
        return next(errors);
    }
    userDao.findOne({ username: userModel.username }, '')
        .then(function (user) {
            userInDb = user;
            return crypto.hashWithSaltAsync(userModel.password, user.salt)

        })
        .then(function (hash) {
            if (hash == userInDb.password) {
                jwt.sign(userInDb, function (err, token) {
                    if (err) {
                        next(err);
                    } else {
                        res.send(token);
                    }
                });
            } else {
                next("username or password wrong");
            }
        })
        .catch(function (err) {
            next("username or password wrong");
        })
}

function validateUserModel(userModel) {
    var errors = [];
    if (!userModel.username) {
        errors.push("username is required!");
    }
    if (!userModel.password) {
        errors.push("password is required!");
    }

    return errors;
}