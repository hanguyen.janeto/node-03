var jwt = require('./../utils/jwt');
var fs = require('fs');
var path = require('path');
var userDao = require('../dao/user.dao');

exports.auth = function () {
    return function (req, res, next) {
        var token = req.headers['x-access-token'];
        if (token) {
            jwt.verify(token).then(function (decodedData) {

                    var name = decodedData.username;
                    userDao.findOne({
                            username: name
                        })
                        .then(function (user) {
                            req.user = user;
                            next();
                        })
                        .catch(function (err) {
                            next({status_code: 404, message: "user not found"})
                        })

                    // fs.readFile(path.join(__dirname, "../user.json"), 'utf8',function (err, data) {
                    //     if (err) {
                    //         res.status(401);
                    //         res.json({
                    //             message: 'Invalid Token'
                    //         });
                    //     } else {
                    //         var listUser = JSON.parse(data);
                    //         var user;
                    //         for (var i = 0; i < listUser.length; i++) {
                    //             if (listUser[i].name == name) {
                    //                 user = listUser[i];
                    //             }
                    //         }
                    //         if (user) {
                    //             req.user = user;
                    //             next();
                    //         } else {
                    //             res.status(401);
                    //             res.json({
                    //                 message: 'Invalid Token'
                    //             });
                    //         }
                    //     }
                    // });
                })
                .catch(function (err) {
                    res.status(401);
                    res.json({
                        message: 'Invalid Token'
                    });
                })
        } else {
            res.status(401);
            res.json({
                message: "Not Authorized"
            });
        }
    }
}