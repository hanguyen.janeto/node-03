var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs');
var path = require('path');
var auth = require('./../middle-ware/auth');
var userController = require('./../controller/user.controller');

// router.get('', getUser);
// router.put('/:id',auth.auth(), userController.updateUser());
// router.get('/:id', getUserById);
router.get("/", userController.getUsers);
// router.post('', createUser);
// router.delete('/:id', deleteUser);
return router;

