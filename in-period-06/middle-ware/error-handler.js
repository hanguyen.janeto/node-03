exports.errorHandler = function () {
    return function (err, req, res, next) {
      console.log("error");
      if (err) {
        res.status(err.statusCode || 500).send({
          message: err.message
        });
      }
    };
  };  