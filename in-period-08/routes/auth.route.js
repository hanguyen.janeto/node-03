var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs');
var path = require('path');
var jwt = require('../utils/jwt');

module.exports = function () {
    router.post('/register', register);
    router.post('/login', login);
    return router;
}

function register(req, res, next) {
    var user = req.body;
    var errors = validateUserModel(user);
    if (errors.length > 0) {
        return next({status:400, message: errors});
    }

    fs.readFile(path.join(__dirname, "../user.json"), "utf8", function (err, data) {
        if (err) {
            res.status(404);
            res.send(err.message);
        } else {
            var formattedData = JSON.parse(data);
            for (var i = 0; i < formattedData.length; i++) {
                if (formattedData[i].username == user.username) {
                    return next("user is exist");
                }
            }

            formattedData.push(user);

            fs.writeFile(path.join(__dirname, "../user.json"), JSON.stringify(formattedData), function (err) {
                if (err) {
                    res.status(500);
                    res.send(err.message);
                } else {
                    res.send(user);
                }
            })
        }
    })
}

function validateUserModel(userModel) {
    var errors = [];
    if (!userModel.username) {
        errors.push("username is required!");
    }
    if (!userModel.password) {
        errors.push("password is required!");
    }

    return errors;
}

function login(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;

    fs.readFile(path.join(__dirname, "../user.json"), 'utf8', function (err, data) {
        var list = JSON.parse(data);
        var user;
        for (var i = 0; i < list.length; i++) {
            if (list[i].username == username && list[i].password == password) {
                user = list[i];
            }
        }

        if (user) {
            jwt.sign({
                name: user.username
            }).then(function(token){
                res.send({
                    access_token: token
                });
            })
            .catch(function(err){
                next(err);
            })
        } else {
            next({status:400, message: "username or password wrong"});
        }

    });
}